# vs-code-settings

My personal Visual Studio Code settings

## My Plugins/Addons
- advance-new-file
- Beautify
- Code Runner
- Dash
- File Utils
- Path Autocomplete
- PHP Intelephense
- snippet-creator
- Sublime Text Keymap and Importer
- TWIG Pack
- WordPress Devlopment
- Slime Theme (Theme)
- Material Icon Theme (Icon Pack)

## My Settings

```json
{
    "window.zoomLevel": 0,

    "editor.fontSize": 16,
    "workbench.colorTheme": "Slime",
    "emmet.includeLanguages": {
        "javascript": "javascriptreact"
    },
    "editor.fontFamily": "Fira Code",
    "editor.fontLigatures": true,
    "workbench.iconTheme": "material-icon-theme",
    "editor.tokenColorCustomizations": {
        "textMateRules": [
          {
            "scope": "comment, comment.block.html",
            "settings": {
              "fontStyle": "italic"
            }
          }
        ]
      },
      "sublimeTextKeymap.promptV3Features": true,

      // Changes the multi cursor mouse binding
      "editor.multiCursorModifier": "ctrlCmd",

      // Specifies the location of snippets in the suggestion widget
      "editor.snippetSuggestions": "top",

      // Controls whether format on paste is on or off
      "editor.formatOnPaste": true,

      // Hide Status bar
      "workbench.statusBar.visible": false,

      // Hide editors
      // "explorer.openEditors.visible": 0,
      "editor.minimap.enabled": false,
      "explorer.confirmDelete": false,
      "editor.tabCompletion": true,

      // Emmet
      "emmet.triggerExpansionOnTab": true,
      "emmet.showSuggestionsAsSnippets": true,
      "editor.snippetSuggestions": "top",
      "liveServer.settings.donotShowInfoMsg": true,

      // Clear previous output | Code Runner
      "code-runner.clearPreviousOutput": true
    }
```
